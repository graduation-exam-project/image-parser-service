const config = {
  func: require("../imageParser"),
  static: {
    name: "image-parser-service",
    port: "8003",
    method: "GET"
  }
};

module.exports = config;
