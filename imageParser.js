const vision = require("@google-cloud/vision");
const path = "../assets/";

async function imageParser(fileName) {
  const client = new vision.ImageAnnotatorClient();

  const [result] = await client.documentTextDetection(path + fileName);
  const fullTextAnnotation = result.fullTextAnnotation;

  return fullTextAnnotation;
}

module.exports = imageParser;
